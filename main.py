from graphlib import TopologicalSorter
from datetime import date
import revdebug

def dictionaryMergeUpdateOperators():
    x = {"key1": "value1 from x", "key2": "value2 from x"}
    y = {"key2": "value2 from y", "key3": "value3 from y"}
    z = x | y
    print(z)

def stringRemovePrefixesAndSuffixes():
    str = "impossible"
    str= str.removeprefix("im")
    print(str)



def typeHintingGenericsInStandardCollections(names: list[str]) -> None:
    for name in names:
        print("Hello", name)

def graphLib():
    graph = {"D": {"B", "C"}, "C": {"A"}, "B": {"A"}}
    ts = TopologicalSorter(graph)
    print(tuple(ts.static_order()))

if __name__ == '__main__':
    dictionaryMergeUpdateOperators()
    stringRemovePrefixesAndSuffixes()
    typeHintingGenericsInStandardCollections(["apple", "banana", "cherry"])
    graphLib()
    revdebug.snapshot("python3.9")


